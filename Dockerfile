# syntax=docker/dockerfile:1

## Build
FROM golang:alpine AS build
WORKDIR /app
COPY . ./
RUN go mod download
RUN go build -o /lmddgtfybot

## Deploy
FROM alpine
WORKDIR /
ENV TOKEN=""
COPY --from=build /lmddgtfybot /
ENTRYPOINT /lmddgtfybot -t ${TOKEN}

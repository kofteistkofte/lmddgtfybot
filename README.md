# lmddgtfybot
lmddgtfybot is a simple Discord bot written in Go with [discordgo](https://github.com/bwmarrin/discordgo) library that creates [lmddgtfy.net](https://lmddgtfy.net) urls from messages started with `!ddg` prefix.

## How to run it

To install lmddgtfybot, you need golang installed in your system or server, added to your PATH, and just run this command:
```
go get gitlab.com/kofteistkofte/lmddgtfybot
```

After installing, you need to provide a token for your instance with `-t` argument:
```
lmddgtfybot -t <your_auth_token>
```

## How to use in Discord
After adding to your server, just send a message with `!ddg` prefix. Example:
```
!ddg this is a test search
```

This will return as `https://lmddgtfy.net/?q=this%20is%20a%20test%20search`

If you want to tag someone, just tag them in your message, bot will also tag them. Example:
```
!ddg this is a test search with user tagging @random_user
```

## ToDo list
- [ ] Add ability to tag users without actually tagging them.

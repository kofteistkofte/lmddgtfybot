package main

import (
	"flag"
	"fmt"
	"net/url"
	"os"
	"os/signal"
	"regexp"
	"strings"
	"syscall"

	"github.com/bwmarrin/discordgo"
)

func init() {
	flag.StringVar(&token, "t", "", "Bot Token")
	flag.Parse()
}

var (
	token  string
	buffer = make([][]byte, 0)
)

func main() {
	if token == "" {
		fmt.Println("Please provide with a valid token with -t argument")
		return
	}

	dg, err := discordgo.New("Bot " + token)
	if err != nil {
		fmt.Println("An error happened during creating a discord session: ", err)
		return
	}
	dg.AddHandler(message_create)
	err = dg.Open()
	if err != nil {
		fmt.Println("An error happened while opening a connection to Discord API: ", err)
		return
	}

	fmt.Println("I'm at your command my master! I'll help you to answer every question!")

	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	dg.Close()
}

func message_create(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == s.State.User.ID {
		return
	}

	if strings.HasPrefix(m.Content, "!ddg") {
		fmt.Println(m.Content)
		message := strings.TrimPrefix(m.Content, "!ddg ")
		re, _ := regexp.Compile(`<@!\d+>`)
		user := re.FindString(message)
		var query string
		var ddg_url string
		if user != "" {
			fmt.Println(user)
			message = strings.TrimSuffix(message, " "+user)
			query = url.QueryEscape(message)
			ddg_url = "https://lmddgtfy.net/?q=" + query + " " + user
		} else {
			query = url.QueryEscape(message)
			ddg_url = "https://lmddgtfy.net/?q=" + query
		}
		s.ChannelMessageSend(m.ChannelID, ddg_url)
	}
}
